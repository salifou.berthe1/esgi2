# Exercice Maven et Junit 



## Exercice 1
Le but de cet exercice est de compter le nombre d'arbre dans paris correspondant au nombre de ligne du fichier [les-arbres-de-paris.csv](datas/les-arbres-de-paris.csv)

1. Dans une classe ``FileLineCountTest.java`` et en utilisant le factory ``java.nio.file.Path``, la m�thode ``Files.lines`` et l'instruction ``try-with-resources`` pour afficher le nombre de lignes du fichier
2. en regardant le documentation du  ``java.nio.file.Path`` dites pourquoi il est recommend� de l'utiliser plut�t que le bon vieux ``java.io.File ``
3. Pouvons nous remplacer l'instruction ``try-with-resources`` par quelle autre instruction ? et pourquoi ``try-with-resources`` est pr�f�rable � cette derni�re ?

## Exercice 2
Le but de cet exercice est de compter le nombre d'arbres par arrondissement
1. cr�er une classe immutable ``ParisTree.java`` avec les champs 
```
long id, String city et String adresse
```
2. create dans la classe ``ParisTree.java`` une m�thode  static ``public static final List<ParisTree> loadList(Path path)``  qui retoure une liste immutable de ParisTree (utilisez la m�thode ``Stream.toList()``)
3. Si l'on veut rechercher un arbre par id, utiliser une java.util.List n'est pas la bonne structure de donn�es, on pr�f�re utiliser une java.util.Map.
�crire une fonction loadMap qui prend en param�tre une liste d'arbres et renvoie une Map non modifiable qui associe � l'id  l'objet ParisTree correspondant.
4. Il existe une m�thode ``Function.identity()``. Comment peut-on l'utiliser dans notre cas ?
5. On cherche � afficher le nombre total d'arbre par arrondissement
5. On cherche � classer les arrondissements par ordre decroissant du nombre d'arbre

